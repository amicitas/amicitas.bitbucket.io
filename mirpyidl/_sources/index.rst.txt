.. mirpyidl documentation master file, created by
   sphinx-quickstart on Tue Dec 17 23:27:06 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _mirpyidl:

.. include:: introduction.rst

Documentation Index
===================

.. toctree::
   :maxdepth: 2

   introduction
   autodoc/mirpyidl
   development
   authors
   license



Indices and Tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

