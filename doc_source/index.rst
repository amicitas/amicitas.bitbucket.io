.. bitbucket.org documentation master file, created by
   sphinx-quickstart on Sat Dec 21 12:40:29 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Amicitas' Bitbucket Projects
============================

`mirpyidl <http://amicitas.bitbucket.io/mirpyidl>`_

    A library to call IDL (Interactive Data Language) from python.  Allows 
    wrapping of IDL routines and objects as well as arbitrary execution of
    IDL code.  Requires a licenced installation of IDL.

`mirmpfit <http://amicitas.bitbucket.io/mirmpfit>`_

    Robust non-linear least squares curve fitting in python.  This is a 
    modified version of the MPFIT package originally written in IDL by 
    Craig Markwardt and based on the MINPACK-1 routines. This modified python 
    version allows seamless integration into object orientied python code.
    In additon this version contains a modifide minimzation scheme which
    allows for the addition of arbitrary avoidance conditions.
